# Automated Testing Notes & Resources

## General

* [Types of Test](test_types.md).
* [Working With Legacy (messy) Test Suites](legacy_tests.md).
* [On “Code Coverage”](code_coverage.md).

## XCTest / Testing Swift Projects

* Code: [Asynchronous test assertion helper](https://gitlab.com/-/snippets/2567566).
  Support testing that an asynchronous function throws an error.
  (Basically, a drop-in replacement for XCTAssertThrowsError when trying to
  test a function that is marked async.)

* [Unit Testing SwiftUI](swiftui/): Notes, tips, and resources/tools.
  * [Working with Bindings](swiftui/bindings.md)

---

[Grant’s Development Resources](/)
