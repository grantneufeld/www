# On “Code Coverage”

Code coverage is a measurement available in most automated testing systems. It identifies which parts of the code run during tests, and which are not. This is often presented as a metric of “coverage percentage”, where 100% coverage means that every part of the code is executed during the tests.

Having 100% code coverage does not mean a test suite is good or complete. It just means that none of the the code is left untouched when tests are run.

Having less than 100% coverage usually means that the test suite is incomplete.

There are some exceptions, such as in cases where there is a call to an external system that should not be included in the test suite (perhaps for security or cost/time reasons), or where quirks of the language/system/framework require the presence of some code that simply can’t be reached. But, in those cases, that “untestable” code should be isolated to a separate function or class that serves as a wrapper which can be stubbed out, or replaced with a mock, during testing.

---

[Grant’s Development Resources](/)
➡ [Testing Notes](/testing/)
