# Working With Legacy (messy) Test Suites

If automated tests are taking a long time, a quick and dirty improvement may be possible by applying parallelization—including over distributed hardware.

But here are some other things that may help, in the order that I would usually apply them to an existing project that wasn’t developed with Test-Driven Development:

* Separate integration/UI/acceptance tests from the unit tests. For moment-to-moment work, unit tests (assuming you have good coverage, right? :wink:) should catch most of the issues that might come up. Save the full suite of tests for when you think you may be ready to make a commit, or have them running on a separate machine (ala “Continuous Integration”).
* Identify integration tests masquerading as unit tests and move them to the integration test suite.
* Identify where some of the common things that can slow tests down are in your tests, and see if you can reduce the number of those tests (consolidation, where possible), or replace  calls to things outside the “subject under test” with stubs/mocks when appropriate—dependency injection can come in handy for this. Common slow downs are generally things that go outside your code (file system access, database calls, network calls, etc.).
* Maybe (not my favourite, but sometimes necessary): Split your large project(s) into modules that can be worked on, and tested, separately. You’ll still need to do integration tests for the overall project, but the split can speed up localized work.
* Get your team more training/reading/learning on test optimization patterns (I last studied that more than a decade ago, and it was Ruby based, so I don’t have relevant resource recommendations currently).
* Get your team into the habit of test-first coding (TDD). Tests are far less likely to get bloated and slow when folks do this well.
* As a last resort, profile your test suite to find the biggest offenders and try to trim them down, or otherwise fix them.

---

[Grant’s Development Resources](/)
➡ [Testing Notes](/testing/)
