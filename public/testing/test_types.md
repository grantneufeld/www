# Types of Test

## Unit Test

Test an aspect of a single function.

## Integration Test

Tests the connections/interface between classes/functions/modules/etc.

## UI Test

Testing gestures and other visual interface interactions.

Do they produce the expected outcomes (measured by interface elements displayed)?

## Acceptance Test (BDD)

* Human-readable (reader is not required to understand code) description of a behaviour (by the code).
* Can serve as the agreement/contract between the developer(s) and the client/business/project-owner.
* [Cucumber](https://cucumber.io/) is a testing tool specifically for acceptance tests. It uses a syntax called “[Gherkin](https://cucumber.io/docs/gherkin/)”. E.g.:

```gherkin
Given I have a "Wikipedia News" Harvester configured,
And Wikipedia has 3 top news stories
When I run the "Wikipedia News" Harvester
Then I should get a list of 3 top news stories.
```

---

[Grant’s Development Resources](/)
➡ [Testing Notes](/testing/)
