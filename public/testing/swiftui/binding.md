# Unit Testing SwiftUI: Working with Bindings

Here’s an approach I’ve found that works for unit testing a view that takes a `@Binding`.

Given a View:

```swift
import SwiftUI

struct MyView: View {
    @Binding var message: String?

    var body: some View {
        if self.message == nil {
            EmptyView()
        } else {
            Text(self.message!)
        }
    }
}
```

Just instantiate the `Binding` using the initializer that takes a `get:` and a `set:` closure parameter (which can reference a local variable in your test method). You can then pass that binding directly when instantiating the view (no need for the `$` prefix).

```swift
@testable import MyApp
import SwiftUI
import XCTest

final class MyViewTests: XCTestCase {
    func test_givenNilMessage_isEmptyView() throws {
        var message: String?
        // ⭐️ This is the key line for this approach:
        let binding = Binding { message } set: { message = $0 }
        let sut = MyView(message: binding)
        // ...
    }
}
```

---

[Grant’s Development Resources](/)
➡ [Testing Notes](/testing/)
➡ [Unit Testing SwiftUI](/testing/swiftui/)
