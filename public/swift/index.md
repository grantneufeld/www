# Swift Language Notes & Resources

* Code: [A Swift Regular Expression for URLs](A_Swift_regex_for_URLs.md).
* Code: [Collection class as wrapper around Array](https://gitlab.com/-/snippets/2592060).
  Has conformance to the MutableCollection and RandomAccessCollection protocols.
* Code: [Separating functions that require the main thread in Swift](https://gitlab.com/-/snippets/2592029).
  (Uses async/await.)


---

[Grant’s Development Resources](/)
